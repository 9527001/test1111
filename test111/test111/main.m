//
//  main.m
//  test111
//
//  Created by 赵东 on 2018/8/21.
//  Copyright © 2018年 赵东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
